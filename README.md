# これは何？
ハイドロポンパーのFWです。ESP8266 向けのArduino IDE 用 ソースコードです。

# ビルド方法
* ボードマネージャーで ESP8266Modules を追加
  * 参考：http://www.humblesoft.com/wiki/?ESP8266%20Arduino%E3%81%AE%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB#ofcf14f8
* ライブラリに ArduinoOSC を追加
  * 参考：https://github.com/hideakitai/ArduinoOSC
* SentouDeviceFW.ino と同じフォルダに「WifiSettings.h」を作成
  * 中身に以下を記述
```
#pragma once
#include <ESP8266WiFi.h>
static const char ssid[] = "（SSIDを記述）";
static const char pass[] = "（Wifiパスワードを記述）";
IPAddress ip(192, 168, 0, 42);
IPAddress gateway(192,168, 0, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress DNS(192, 168, 0, 1);
```
* 以下の通りビルド設定する
  * 参考：http://www.humblesoft.com/wiki/?ESP8266%20Arduino%E3%81%AE%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB#m5524c26
* 以上
