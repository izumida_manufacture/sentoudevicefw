#include <WiFiUdp.h>
#include <ArduinoOSC.h>
#include <Ticker.h>

/* ==========
 *  WifiSettings.h を作り中に以下の形でネットワーク設定を記載する
 *  #pragma once
 *  #include <ESP8266WiFi.h>
 *  static const char ssid[] = "SSID";
 *  static const char pass[] = "PASS";
 *  IPAddress ip(192, 168, 0, 40);
 *  IPAddress gateway(192,168, 0, 1);
 *  IPAddress subnet(255, 255, 255, 0);
 *  IPAddress DNS(192, 168, 0, 1);
*/
#include "WifiSettings.h"

#define PUMP_OUT_PIN  (0)
#define WATER_IN_PIN  (14)

WiFiUDP udp;
ArduinoOSCWiFi osc;

#define AOUT_MAX    (755)
#define AOUT_MIN    (256)
#define AOUT_RANGE  (AOUT_MAX - AOUT_MIN)

int nowMode = 0;
int nowPower = AOUT_MAX;
int nowShot = 0;

int nowPush = 0;

Ticker ticker;

uint32_t tick = 0;
uint32_t startCount = 0;

void onTick() {
  if (nowShot || nowPush) {
    int power = nowPower;
    if ((tick - startCount) < 200) {
      power = (tick - startCount) * nowPower / 200;
    }
    
    switch (nowMode) {
      case 0:
        analogWrite(PUMP_OUT_PIN, power);
        break;
  
      case 1:
        if ((tick % 350) <= 300) {
          analogWrite(PUMP_OUT_PIN, power);
        } else {
          analogWrite(PUMP_OUT_PIN, 0);
          startCount = tick;
        }
        break;
    }
    
  } else {
    analogWrite(PUMP_OUT_PIN, 0);
    
  }

  tick++;
}

void setup() {
  // put your setup code here, to run once:
  pinMode(PUMP_OUT_PIN, OUTPUT);
  digitalWrite(PUMP_OUT_PIN, LOW);
  
  pinMode(WATER_IN_PIN, INPUT);

  Serial.begin(115200);
  Serial.println("===== Wake up =====");
  Serial.println("");

  Serial.println("WiFi Connecting...");
  WiFi.mode(WIFI_STA);
  WiFi.config(ip, gateway, subnet, DNS); //static_ip
  delay(100);
  WiFi.begin(ssid, pass);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  Serial.println("Connection Success!");
  Serial.println("");
  Serial.print("IP:");
  Serial.println(WiFi.localIP());
  Serial.println("");

  osc.begin(udp, 12345);
  osc.addCallback("/device/mode", &onOSCModeCmd);
  osc.addCallback("/device/power", &onOSCPowerCmd);
  osc.addCallback("/device/shot", &onOSCShotCmd);

  ticker.attach_ms(1, onTick);

  Serial.println("Settings Done!");
}

void loop() {
  // put your main code here, to run repeatedly:

  osc.parse();
  
  int push = (digitalRead(WATER_IN_PIN) == LOW) ? 1 : 0;
  if (!nowPush && push) {
    startCount = tick;
  }
  nowPush = push;
}

void onOSCModeCmd(OSCMessage& m) {
  int mode = m.getArgAsInt32(0);
  Serial.print("rec: /device/mode ");
  Serial.println(mode);
  nowMode = (mode == 0) ? 0 : 1;
}

void onOSCPowerCmd(OSCMessage& m) {
  int power = m.getArgAsInt32(0);
  Serial.print("rec: /device/power ");
  Serial.println(power);
  power = (power > 100) ? 100 : power;
  power = (power < 0) ? 0 : power;
  nowPower = power * AOUT_RANGE / 100 + AOUT_MIN;
}

void onOSCShotCmd(OSCMessage& m) {
  int shot = m.getArgAsInt32(0);
  Serial.print("rec: /device/shot ");
  Serial.println(shot);
  nowShot = (shot == 0) ? 0 : 1;
  startCount = tick;
}
